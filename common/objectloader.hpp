#ifndef OBJECTLOADER_H
#define OBJECTLOADER_H
#include <GL/glew.h>

#include <glfw3.h>
bool loadOBJ
(
	const char* path,
	std::vector<glm::vec3> &out_vertices
	//std::vector<glm::vec2> &out_uvs,
	//std::vector<glm::vec3> &out_normals
	);

bool loadLOC
(
	const char *name,
	std::vector<glm::vec3> &points 
);

bool loadGRASP
(
	const char *name,
	std::vector<glm::vec3> &grasp_points 
);


bool loadpath
(
	const char* path,
	std::vector<glm::vec3> &path_trajectory,int *size, std::vector <int>&temp_value,std::vector<glm::vec3> &color_trajectory,std::vector<glm::vec3> &alpha_value,int* dst_node,std::vector<glm::vec3> &ellipse_loc,int lookup[][8],int lookup_ellipse[][8],std::vector<glm::vec3>&ellipse_scale
	
	);

GLuint loadBMP_custom(const char * imagepath);


#endif
