#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>
#include <fstream>
#include <iterator>
#include <sstream>
#include <glm/glm.hpp>
#include <iostream>
#include "objectloader.hpp"
#include <GL/glew.h>
#include <math.h>
#include <glfw3.h>


#define PI 3.14159265

bool loadOBJ(
	const char * path, 
	std::vector<glm::vec3> & out_vertices
	//std::vector<glm::vec2> & out_uvs,
	//std::vector<glm::vec3> & out_normals
){
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices; 
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE * file = fopen(path, "r");
	if( file == NULL ){
		printf("Impossible to open the file \n");
		getchar();
		return false;
	}

	while( 1 ){

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader
		
		if ( strcmp( lineHeader, "v" ) == 0 ){
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
			temp_vertices.push_back(vertex);
		
		} else if ( strcmp( lineHeader, "f" ) == 0 ){
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			//int matches = fscanf(file, "%d %d %d\n", &vertexIndex[0],  &vertexIndex[1], &vertexIndex[2] );
			

	int matches = fscanf(file, "%d//%d %d//%d %d//%d\n", &vertexIndex[0], &normalIndex[0], &vertexIndex[1], &normalIndex[1], &vertexIndex[2], &normalIndex[2] );
			if (matches != 6){
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]); // ei 3 ta line matro comment out korlam to check 

			//currently not using them
			/*uvIndices    .push_back(uvIndex[0]);
			uvIndices    .push_back(uvIndex[1]);
			uvIndices    .push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);*/
		}else{
			// nothing
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for( unsigned int i=0; i<vertexIndices.size(); i++ ){

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		//unsigned int uvIndex = uvIndices[i];
		//unsigned int normalIndex = normalIndices[i];
		
		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[ vertexIndex-1 ];
		//glm::vec2 uv = temp_uvs[ uvIndex-1 ];
		//glm::vec3 normal = temp_normals[ normalIndex-1 ];
		
		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		//out_uvs     .push_back(uv);
		//out_normals .push_back(normal);
	
	}

	return true;
}

bool loadLOC(const char *name,
	std::vector<glm::vec3> & points ) {

	printf("Loading file %s...\n", name);
	std::vector<glm::vec3> temp_vertices; 


	FILE * file = fopen(name, "r");
	if( file == NULL ){
		printf("Impossible to open the file \n");
		getchar();
		return false;
	}

	int temp=8;
	int i=0;
	int node;
		
			for ( i=0;i<temp;i++)
		
			{	glm::vec3 vertex;
				fscanf(file, "%d %f %f %f\n", &node,&vertex.x, &vertex.y, &vertex.z );
				//std::cout<<"printing value at : "<<vertex.x<<" "<< vertex.y<<" "<<vertex.z<<std::endl;
				temp_vertices.push_back(vertex);
				points.push_back(vertex);
				
				

			}	
         //printf("size of points vector: %d \n", points.size());
	

	
	
	return true;

	

}
bool loadGRASP(const char *name,
	std::vector<glm::vec3> & grasp_points ) {

	printf("Loading file %s...\n", name);
	std::vector<glm::vec3> temp_vertices; 


	FILE * file = fopen(name, "r");
	if( file == NULL ){
		printf("Impossible to open the file \n");
		getchar();
		return false;
	}

	int temp=8;
	int i=0;
	int node;
		
			for ( i=0;i<temp;i++)
		
			{	glm::vec3 vertex;
				fscanf(file, "%d %f %f %f\n", &node,&vertex.x, &vertex.y, &vertex.z );
				//std::cout<<"printing value at : "<<vertex.x<<" "<< vertex.y<<" "<<vertex.z<<std::endl;
				temp_vertices.push_back(vertex);
				grasp_points.push_back(vertex);
				
				

			}	
         //printf("size of points vector: %d \n", points.size());
	

	
	
	return true;

	

}

glm::vec3 getPoints(int node)
{
	glm::vec3 vertex;
	switch(node)
	{
		case 0:vertex.x=0.42749;
			vertex.y=-0.307435;
			vertex.z=1.10724;
			break;
		case 1:vertex.x=0.326072;
			vertex.y=0.348141;
			vertex.z=1.4876	;
			break;

		case 2:vertex.x=0.24433;
			vertex.y=-0.205084;
			vertex.z=1.5241;
			break;

		case 3:vertex.x=-0.237284;
			vertex.y=-0.305678;
			vertex.z=0.977862;
			break;

		case 4:vertex.x=0.227728;
			vertex.y=-0.345997;
			vertex.z=1.0278;
			break;

		case 5:vertex.x=0.220413;
			vertex.y=-0.15514;
			vertex.z=1.80477;
			break;

		case 6:vertex.x=0.0523754;
			vertex.y=0.252256 ;
			vertex.z=1.27541;
			break;

		case 7:vertex.x=-0.298511 ;
			vertex.y=-0.371976 ;
			vertex.z=1.80589;
			break;

		 default: std::cout<<"not working"<<std::endl;
		

	}
		return vertex;
}





bool loadpath(
	const char * path, 
	std::vector<glm::vec3> &path_trajectory,int *size, std::vector<int>&temp_value,std::vector<glm::vec3> &color_trajectory,std::vector<glm::vec3> &alpha_value,int* dst_node,std::vector<glm::vec3> &ellipse_loc,int lookup[][8],int lookup_ellipse[][8],std::vector<glm::vec3>&ellipse_scale
	
){
	std::ifstream locArea("Data2/Scene_Moveface/CUP/0/location_area.txt");	
	std::string line;
	std::vector<int> locAreaNode; 
	while(std::getline(locArea, line))	//Getting the next line
	{
		std::stringstream ss(line);
		int node;
		ss>>node;
		locAreaNode.push_back(node);
	}	
	
	


	std::ifstream file("../files/graph.txt");

	
	
	int index=-1;     //this will store the index value
	int index_ellipse =-1; //this will store the index value for the ellipses , to know from which index each path starts
	//temp_value.push_back(index);
	int index_copy;  //will store the index value for the dst_node array to know where to change the index values of the alpha_value array
	bool check=0;    //to check if the value is read then we will read the seventh line
	
	
	
	//reading the first line
	while(std::getline(file, line))	//Source Node, Destination Node, something node
	{
		std::stringstream sss(line);
		int source_line, destination_line,source_node, destination_node;
		sss>>source_line;
		sss.ignore();
		sss>>destination_line;
		sss.ignore();
		
			
			//std::cout<<"source_node : "<<source_node<<std::endl;
			source_node = locAreaNode[source_line];
			destination_node = locAreaNode[destination_line];
			glm::vec3 extra_point=getPoints(source_node);
			glm::vec3 dekhtesi_last_node=getPoints(destination_node);
	
//			Reading the second line
			std::getline(file, line);


//			Reading the third line
			std::getline(file,line);


//			Reading the fourth line
			std::getline(file,line);
			std::stringstream ss(line);
		
			glm::vec3 vertex;  //temp vec to store value
			glm::vec3 color;
			color.x=1.0f;
			color.y=0.0f;
			color.z=0.0f;

			int abc=0;
			glm::vec3 alpha;
			if (source_node == 1)
			{	alpha.x=0.2;
				alpha.y=1.0;
				alpha.z=1.0;
			}
			else
			{
				alpha.x=0.2;
				alpha.y=0.3;
				alpha.z=0.3;
			}
			
			glm::vec4 temp;
			ss>>temp.x;
			ss.ignore();
			ss>>temp.y;
			ss.ignore();
			ss>>temp.z;
			ss.ignore();
			ss>>temp.w;
			ss.ignore();
		
			if ( temp.x == 0.000000 && temp.y==0.000000 && temp.z==0.000000)
		{
				
		}	
		else 
		{
			check=1;			
			//std::cout<<" extra points x : "<<extra_point.x<<std::endl;
			vertex.x = (-temp.x * temp.w) + extra_point.x;
			vertex.y = (-temp.y  * temp.w)+extra_point.y;
			vertex.z = (temp.z  * temp.w)+extra_point.z;
								
			path_trajectory.push_back(vertex);
			ellipse_loc.push_back(vertex);
			color_trajectory.push_back(color);
			alpha_value.push_back(alpha);				
			
			index++;
			abc++;
			//index_ellipse++;
			//std::cout<<"index_ellipse:	"<<index_ellipse<<std::endl;		
			temp_value.push_back(index);
					
			dst_node[destination_node]= index;
			lookup[source_node][destination_node]=index;
			//lookup_ellipse[source_node][destination_node]=index_ellipse;
			
						
			//std::cout<<"dst node number :"<<destination_node<<"\tvalue : "<<index<<std::endl;	
	
			while(ss>>temp.x)
			{
				ss.ignore();
				ss>>temp.y;
				ss.ignore();
				ss>>temp.z;
				ss.ignore();
				ss>>temp.w;
				ss.ignore();
				
				vertex.x = (-temp.x * temp.w)+ extra_point.x;
				vertex.y = (-temp.y * temp.w)+extra_point.y;
				vertex.z = (temp.z * temp.w)+extra_point.z;
			
				//std::cout << temp.x<<" "<< temp.y<<" "<< temp.z<<" ";
				path_trajectory.push_back(vertex);
				color_trajectory.push_back(color);

				if ( abc==2 || abc==50 || abc==80)

				{
					ellipse_loc.push_back(vertex);
					index_ellipse++;
				}
				abc++;
				lookup_ellipse[source_node][destination_node]=index_ellipse;
				
				
				alpha_value.push_back(alpha);
				//std::cout<<"alpha value from file reader at [100][0] "<<alpha_value[100][0]<<std::endl;
				index++;
			}
			
			//path_trajectory.push_back(dekhtesi_last_node);
			//color_trajectory.push_back(color);
			//alpha_value.push_back(alpha);
			//index++;
			(*size)++;
			// temp_value.push_back(index);
			//std::cout<<"size value"<<*size<<std::endl;
		}
		
//		Reading the fifth line
		std::getline(file, line);

//		Reading the sixth line
		std::getline(file, line);

//		Reading the seventh line
		std::getline(file, line);
		std::stringstream rr(line);
		if(check==1)
	{	float value_sector_map; //will read the value from the line
		std::vector<float>array_first,array_first_a;
		std::vector<float>array_second,array_second_a;
		std::vector<float>array_third,array_third_a;
		int index;
		float sin_value=10;
		
		int sector_map_counter=0;//will count the index number of the values in the line
		while(rr>>value_sector_map)
			{
				sector_map_counter++;
				rr.ignore();

				if(sector_map_counter>35 && sector_map_counter<72)
				array_first.push_back(value_sector_map);

				if(sector_map_counter>1799 && sector_map_counter<1836)
				array_second.push_back(value_sector_map);

				if(sector_map_counter>2880 && sector_map_counter<2917)
				array_third.push_back(value_sector_map);
			}
			//std::cout<<"value of sector_map_counter:"<<sector_map_counter<<std::endl;
			//std::cout<<"array size:"<<array_third.size()<<std::endl;
			float highest_temp=array_first[0];
			for(int i=0;i<array_first.size();i++)
			{
				if(highest_temp<array_first[i])
				{	highest_temp=array_first[i];
					index=i;
				}
			}
			for(int i=index+1;i<array_first.size();i++)
			{
				array_first_a.push_back(array_first[i]);
			}
			for(int i=0;i<index;i++)
			{
				array_first_a.push_back(array_first[i]);
			}

			for(int i=0;i<array_first_a.size();i++)
			{
				//std::cout<<"old: "<<array_first_a[i]<<std::endl;
				array_first_a[i]=array_first_a[i]*sin(sin_value*PI/180);
				//std::cout<<"new: "<<array_first_a[i]<<std::endl;
				sin_value=sin_value+10;
			}
			float highest_temp_a=array_first_a[0];
			for(int i=0;i<array_first_a.size();i++)
			{
				if(highest_temp<array_first_a[i])
				{	highest_temp_a=array_first_a[i];
					
				}
			}
			//std::cout<<"first: "<<highest_temp_a<<std::endl;
			ellipse_scale.push_back(glm::vec3(highest_temp_a,highest_temp,1.0));
			//for the second reading
			sin_value=10;

			highest_temp=array_second[0];
			for(int i=0;i<array_second.size();i++)
			{
				if(highest_temp<array_second[i])
				{	highest_temp=array_second[i];
					index=i;
				}
			}
			for(int i=index+1;i<array_second.size();i++)
			{
				array_second_a.push_back(array_second[i]);
			}
			for(int i=0;i<index;i++)
			{
				array_second_a.push_back(array_second[i]);
			}

			for(int i=0;i<array_second_a.size();i++)
			{
				//std::cout<<"old: "<<array_first_a[i]<<std::endl;
				array_second_a[i]=std::abs(array_second_a[i]*sin(sin_value*PI/180));
				//std::cout<<"sin value of : "<<sin_value<<": "<<sin(sin_value*PI/180)<<std::endl;
				//std::cout<<"new: "<<array_second_a[i]<<std::endl;
				sin_value=sin_value+10;
			}
			 highest_temp_a=array_second_a[0];
			for(int i=0;i<array_second_a.size();i++)
			{
				if(highest_temp<array_second_a[i])
				{	highest_temp_a=array_second_a[i];
					
				}
			}
			//std::cout<<"second: "<<highest_temp_a<<std::endl;
			
			//std::cout<<"abs of -3,14  "<<std::abs(ab)<<std::endl;
			ellipse_scale.push_back(glm::vec3(highest_temp_a,highest_temp,1.0));
			//for the third reading
			sin_value=10;

			highest_temp=array_third[0];
			for(int i=0;i<array_third.size();i++)
			{
				if(highest_temp<array_third[i])
				{	highest_temp=array_third[i];
					index=i;
				}
			}
			for(int i=index+1;i<array_third.size();i++)
			{
				array_third_a.push_back(array_third[i]);
			}
			for(int i=0;i<index;i++)
			{
				array_third_a.push_back(array_third[i]);
			}

			for(int i=0;i<array_third_a.size();i++)
			{
				//std::cout<<"old: "<<array_first_a[i]<<std::endl;
				array_third_a[i]=std::abs(array_third_a[i]*sin(sin_value*PI/180));
				//std::cout<<"sin value of : "<<sin_value<<": "<<sin(sin_value*PI/180)<<std::endl;
				//std::cout<<"new: "<<array_second_a[i]<<std::endl;
				sin_value=sin_value+10;
			}
			 highest_temp_a=array_third_a[0];
			for(int i=0;i<array_third_a.size();i++)
			{
				if(highest_temp<array_third_a[i])
				{	highest_temp_a=array_third_a[i];
					
				}
			}
			//std::cout<<"third: "<<highest_temp_a<<std::endl;
			
			
			//ellipse_scale.push_back(glm::vec3(highest_temp_a,highest_temp,1.0));
			ellipse_scale.push_back(glm::vec3(highest_temp,highest_temp,1.0));


			//std::cout<<"size of ellipse scale :"<<ellipse_scale.size()<<std::endl;

			
		}
		check=0;	
		
	}
	
	return true;
}

GLuint loadBMP_custom(const char * imagepath){

	printf("Reading image %s\n", imagepath);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(imagepath,"rb");
	if (!file){
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath);
		getchar();
		return 0;
	}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if ( fread(header, 1, 54, file)!=54 ){ 
		printf("Not a correct BMP file\n");
		fclose(file);
		return 0;
	}
	// A BMP files always begins with "BM"
	if ( header[0]!='B' || header[1]!='M' ){
		printf("Not a correct BMP file\n");
		fclose(file);
		return 0;
	}
	// Make sure this is a 24bpp file
	if ( *(int*)&(header[0x1E])!=0  )         {printf("Not a correct BMP file\n");    fclose(file); return 0;}
	if ( *(int*)&(header[0x1C])!=24 )         {printf("Not a correct BMP file\n");    fclose(file); return 0;}

	// Read the information about the image
	dataPos    = *(int*)&(header[0x0A]);
	imageSize  = *(int*)&(header[0x22]);
	width      = *(int*)&(header[0x12]);
	height     = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize==0)    imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos==0)      dataPos=54; // The BMP header is done that way

	// Create a buffer
	data = new unsigned char [imageSize];

	// Read the actual data from the file into the buffer
	fread(data,1,imageSize,file);

	// Everything is in memory now, the file can be closed.
	fclose (file);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);
	
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete [] data;

	// Poor filtering, or ...
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

	// ... nice trilinear filtering ...
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	// ... which requires mipmaps. Generate them automatically.
	glGenerateMipmap(GL_TEXTURE_2D);

	// Return the ID of the texture we just created
	return textureID;
}



//#endif
