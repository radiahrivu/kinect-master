/*


Trajectory parser
Author: Rivu


*/
#include <fstream>
#include <iterator>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>

#include <glm/glm.hpp>



bool loadpath(
	const char * path, 
	std::vector<glm::vec3> &path_trajectory,int *size, std::vector<int>&temp_value,std::vector<glm::vec3> &color_trajectory
	
){
	
	std::ifstream file("trajectory.txt");
	//std::cout<<"Hello from graphparser.cpp"<<std::endl;
	//std::vector<std::vector<double>> vv;
	std::string line;
	
	int index=-1; //this will store the index value
	//temp_value.push_back(index);
	
	
	
	
	//reading the first line
	while(std::getline(file, line))
	{
//		reading	the second and third line(we use this line)
		std::getline(file, line);
		std::getline(file, line);
		//std::cout<<line<<std::endl;
		std::stringstream ss(line); // picks up every third line
		//std::stringstream ff;
		//ff << ss.rdbuf(); //copying the string from ss to ff just to verify j path ase kina
		

		glm::vec3 vertex;  //temp vec to store value
		glm::vec3 color;
			color.x=0.0f;
			color.y=1.0f;
			color.z=0.0f;
		
		glm::vec4 temp;
					ss>>temp.x;
					ss.ignore();
					ss>>temp.y;
					ss.ignore();
					ss>>temp.z;
					ss.ignore();
					ss>>temp.w;
					ss.ignore();
		
	//	if ( temp.x == 0.000000 && temp.y==0.000000 && temp.z==0.000000)
	//	{
				
	//	}	
	//	else 

		{
					vertex.x = temp.x * temp.w;
						vertex.y = temp.y * temp.w;
						vertex.z = temp.z * temp.w;
			
						//std::cout << temp.x<<" "<< temp.y<<" "<< temp.z<<" ";
						path_trajectory.push_back(vertex);
						
						index++;
						temp_value.push_back(index);
						


				
					while(ss>>temp.x)
					{
			
			
			
					ss.ignore();
					ss>>temp.y;
					ss.ignore();
					ss>>temp.z;
					ss.ignore();
					ss>>temp.w;
					ss.ignore();
							
			
		
						vertex.x = temp.x * temp.w;
						vertex.y = temp.y * temp.w;
						vertex.z = temp.z * temp.w;
			
						//std::cout << temp.x<<" "<< temp.y<<" "<< temp.z<<" ";
						path_trajectory.push_back(vertex);
						color_trajectory.push_back(color);
						index++;
			
			
					}
					(*size)++;
					// temp_value.push_back(index);
					//std::cout<<"size value"<<*size<<std::endl;
					

		}
		
		

		
		std::getline(file, line);
		
		
	}


	
	return true;
}

