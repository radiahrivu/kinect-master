/*
 * DataFilter.h
 *
 *  Created on: Apr 19, 2017
 *      Author: chen
 */

#ifndef OPENGLVIEWER_H_
#define OPENGLVIEWER_H_

#include <iostream>
#include <GL/glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>
using namespace glm;
#include <common/graphparser.hpp>
#include <common/location_parser.hpp>
#include <common/objectloader.hpp>
#include <common/shader.hpp>
#include <common/quaternion_utils.hpp>

class OpenGLViewer
{
public:
	OpenGLViewer();
	virtual ~OpenGLViewer();

	virtual void Initialize(
		 	GLuint &VertexArrayID,
			GLuint &vertexbuffer,
			GLuint &colorbuffer,
			GLuint &vertexbufferAxes,
			GLuint &colorbufferAxes,
			GLuint &programID,
			GLuint &MatrixID,
			GLuint &ViewMatrixID,
			GLuint &ModelMatrixID,
			GLuint &TextureID,
			GLuint &colorbuffer1,
			GLuint &circlebuffer,
			GLuint &trajectorybuffer,
			GLuint &trajectory_color_buffer,
			GLuint &alpha_buffer,
			GLuint &grasp,
			GLuint &cube_buffer,
			GLuint &cube_color,
			GLuint &cube_alpha,
			GLuint &Texture,
			GLuint &Texture_second,
			GLuint &Texture_third,
			GLuint &functionality_buffer,
			GLuint &functionality_uv_buffer,
			GLuint &zoom_line_buffer,
			int *dst_node,	
			glm::mat4 &ViewMatrix,
			glm::mat4 &ViewMatrix_second,
			glm::mat4 &ProjectionMatrix,
			std::vector<glm::vec3> &vertices,
			std::vector<glm::vec3> &location_points,
			std::vector<glm::vec3> &trajectory_points, 				
			std::vector<glm::vec3> &trajectory_color,
			std::vector<glm::vec3> &store,
			std::vector<int> &temp_value,
			std::vector<glm::vec3>&alpha_value,
			std::vector<glm::vec3>&grasp_points,
			std::vector<glm::vec3>&cube_points,
			std::vector<glm::vec3>&cube_alpha_points,
			int              &size_array,
			std::vector<glm::vec3> &ellipse_loc,
			std::vector<glm::vec3>&ellipse_scale,
			int lookup[][8],
			int lookup_ellipse[][8],
			const std::string &obj_);
};

#endif /* OPENGLVIEWER_H_ */

