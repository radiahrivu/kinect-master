/*
 * DataFilter.cpp
 *
 *  Created on: Apr 19, 2017
 *      Author: chen
 */

#include "OpenGLViewer.h"

OpenGLViewer::OpenGLViewer() { }

OpenGLViewer::~OpenGLViewer() { }

void OpenGLViewer::Initialize(
	 	GLuint &VertexArrayID,
		GLuint &vertexbuffer,
		GLuint &colorbuffer,
		GLuint &vertexbufferAxes,
		GLuint &colorbufferAxes,
		GLuint &programID,
		GLuint &MatrixID,
		GLuint &ViewMatrixID,
		GLuint &ModelMatrixID,
		GLuint &TextureID,
		GLuint &colorbuffer1,
		GLuint &circlebuffer,
		GLuint &trajectorybuffer,
		GLuint &trajectory_color_buffer,
		GLuint &alpha_buffer,
		GLuint &grasp,
		GLuint &cube_buffer,
		GLuint &cube_color,
		GLuint &cube_alpha,
		GLuint &Texture,
		GLuint &Texture_second,
		GLuint &Texture_third,
		GLuint &functionality_buffer,
		GLuint &functionality_uv_buffer,
		GLuint &zoom_line_buffer,
		int *dst_node,
		glm::mat4 &ViewMatrix,
		glm::mat4 &ViewMatrix_second,
		glm::mat4 &ProjectionMatrix,
		std::vector<glm::vec3> &vertices,
		std::vector<glm::vec3> &location_points,
		std::vector<glm::vec3> &trajectory_points, 				
		std::vector<glm::vec3> &trajectory_color,
		std::vector<glm::vec3> &store,
		std::vector<int> &temp_value,
		std::vector<glm::vec3>&alpha_value,
		std::vector<glm::vec3>&grasp_points,
		std::vector<glm::vec3>&cube_points,
		std::vector<glm::vec3>&cube_alpha_points,
		int              &size_array,
		std::vector<glm::vec3>&ellipse_loc,
		std::vector<glm::vec3>&ellipse_scale,
		int lookup[][8],
		int lookup_ellipse[][8],
		const std::string &obj_)
{
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	
	// [OBJ] *******************************************************************
	bool res = loadOBJ(std::string("../object_models/" + obj_ + ".obj").c_str(), vertices);
	//bool res=loadOBJ("../files/cube.obj",vertices);

	std::vector<glm::vec3> verticesC;
	for (int i=0;i<vertices.size();i++) verticesC.push_back(glm::vec3(0.0f,1.0f,0.0f));

	//FOR THE object
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	//For the Color
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, verticesC.size() * sizeof(glm::vec3), &verticesC[0], GL_STATIC_DRAW);
	// ******************************************************************* [OBJ]
	// [CUBE]*******************************************************************

	bool res4 =loadOBJ("../files/cube.obj",cube_points);
	
	std::vector<glm::vec3>cubeC;
	glm::vec3 alpha_cube;
	alpha_cube.x=0.3;
	alpha_cube.y=0.0;
	alpha_cube.z=0.0;
	
	for(int i=0;i<cube_points.size();i++)				//putting the color value blue for the cube
	cubeC.push_back(glm::vec3(0.0f,0.0f,1.0f));

	for(int i=0;i<cube_points.size();i++)				//putting the alpha value 0.5 for the cube
	cube_alpha_points.push_back(alpha_cube);
	
	
	//for the drawing of the cube
	glGenBuffers(1,&cube_buffer);
	glBindBuffer(GL_ARRAY_BUFFER,cube_buffer);
	glBufferData(GL_ARRAY_BUFFER,cube_points.size()*sizeof(glm::vec3),&cube_points[0],GL_STATIC_DRAW);

	//for the color of the cube
	glGenBuffers(1, &cube_color);
	glBindBuffer(GL_ARRAY_BUFFER, cube_color);
	glBufferData(GL_ARRAY_BUFFER, cubeC.size() * sizeof(glm::vec3), &cubeC[0], GL_STATIC_DRAW);

	//for the alpha of the cube
	glGenBuffers(1,&cube_alpha);
	glBindBuffer(GL_ARRAY_BUFFER,cube_alpha);
	glBufferData(GL_ARRAY_BUFFER,cube_alpha_points.size()*sizeof(glm::vec3),&cube_alpha_points[0],GL_STATIC_DRAW);




	//********************************************************************************
	//[ZOOM LINES]********************************************************************

		static const GLfloat zoom_lines[] = { 
		0.4,0.3,1.5,
		-0.5,0.5,1.5,
		0.4,0.3,1.5,
		-0.5,0.3,1.5
		
	};

	glGenBuffers(1, &zoom_line_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, zoom_line_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(zoom_lines), zoom_lines, GL_STATIC_DRAW);




	//********************************************************************************
	//[FUNCTIONALITY]*****************************************************************

		//Texture = loadBMP_custom("../files/marbles.bmp");
		//Texture = loadBMP_custom("../files/java.bmp");
		Texture = loadBMP_custom("../files/java.bmp");
		Texture_second= loadBMP_custom("../files/desk.bmp");
		Texture_third= loadBMP_custom("../files/desk.bmp");
		
		static const GLfloat functionality_buffer_data[] = {  // draws the object
	
		-2,0,-2,
		2,0,-2,
		-2,2,-2,
		2,0,-2,
		-2,2,-2,
		2,2,-2

		

		
	};

		static const GLfloat functionality_uv_buffer_data[] = { //maps the image to the object



		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f
		 
	};

	
	glGenBuffers(1, &functionality_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, functionality_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(functionality_buffer_data), functionality_buffer_data, GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER,cube_points.size()*sizeof(glm::vec3),&cube_points[0],GL_STATIC_DRAW);

	
	glGenBuffers(1, &functionality_uv_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, functionality_uv_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(functionality_uv_buffer_data), functionality_uv_buffer_data, GL_STATIC_DRAW);






	//*************************************************************************
	// [AXES] ******************************************************************
	static const GLfloat g_color_buffer_data[] = { 
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,
	};
	static const GLfloat g_vertex_buffer_data[] = { 
		0.0f, 0.0f, 0.0f,
		0.2f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.2f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.2f,
	};

	//for axes
	glGenBuffers(1, &vertexbufferAxes);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbufferAxes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
	
	//color
	glGenBuffers(1, &colorbufferAxes);
	glBindBuffer(GL_ARRAY_BUFFER, colorbufferAxes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
	// ****************************************************************** [AXES]


	// [LOCATION POINTS]************************************************************

	static const GLfloat g_color_buffer_data1[] = { // for the color of the grasping points, this is arbritray and nothing fixed yet
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		
		
	};

	bool res1=loadLOC("../files/location_points.txt",location_points);
		//std::cout<<"location_point size : "<<grasp_points.size()<<std::endl;
		
	glGenBuffers(1,&circlebuffer);
	glBindBuffer(GL_ARRAY_BUFFER,circlebuffer);
	glBufferData(GL_ARRAY_BUFFER,location_points.size() * sizeof(glm::vec3), &location_points[0],GL_STATIC_DRAW);	//this one is generating the buffer array with location points

	glGenBuffers(1, &colorbuffer1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data1), g_color_buffer_data1, GL_STATIC_DRAW);    		//this one is generating color buffer for the location points

	



	//************************************************************************************


	//[POSSIBLE TRAJECTORIES]*************************************************************

	
	bool res2=loadpath("../files/graph.txt",store,&size_array,temp_value,trajectory_color,alpha_value,dst_node,ellipse_loc,lookup,lookup_ellipse,ellipse_scale);

	//std::cout<<"size of alpha_value : "<<alpha_value.size()<<std::endl;
	//std::cout<<"size of store: "<<store.size()<<std::endl;
	//std::cout<<"size of the ellipse_loc : "<<ellipse_loc.size()<<std::endl;
	
	
	glGenBuffers(1,&trajectorybuffer);
	glBindBuffer(GL_ARRAY_BUFFER,trajectorybuffer);
	glBufferData(GL_ARRAY_BUFFER,store.size()*sizeof(glm::vec3),&store[0],GL_STATIC_DRAW);			//this one is generating buffer with the possible trajectories

	glGenBuffers(1,&alpha_buffer);
	glBindBuffer(GL_ARRAY_BUFFER,alpha_buffer);
	glBufferData(GL_ARRAY_BUFFER,alpha_value.size()*sizeof(glm::vec3),&alpha_value[0],GL_DYNAMIC_DRAW); //this one is generating the alpha value for the trajectories		
	
	
	glGenBuffers(1,&trajectory_color_buffer);
	glBindBuffer(GL_ARRAY_BUFFER,trajectory_color_buffer);
	glBufferData(GL_ARRAY_BUFFER,trajectory_color.size()*sizeof(glm::vec3),&trajectory_color[0], GL_STATIC_DRAW); //this one is generating color buffer for the possible trajectories

	




	//*********************************************************************************

	//[GRASP POINTS]**********************************************************************
	
	bool res3=loadGRASP("../files/grasp_points.txt",grasp_points);
	//std::cout<<"size of grasp_points : "<<grasp_points.size()<<std::endl;
	
	glGenBuffers(1,&grasp);
	glBindBuffer(GL_ARRAY_BUFFER,grasp);
	glBufferData(GL_ARRAY_BUFFER,grasp_points.size() * sizeof(glm::vec3), &grasp_points[0],GL_STATIC_DRAW);		//this one is generating the buffer array with grasp points


	//************************************************************************************



	// Create and compile our GLSL program from the shaders
	programID =
			LoadShaders(
					"../common/TransformVertexShader.vertexshader",
					"../common/ColorFragmentShader.fragmentshader" );

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	TextureID  = glGetUniformLocation(programID, "myTextureSampler");	
	//ViewMatrixID = glGetUniformLocation(programID, "V");
	//ModelMatrixID = glGetUniformLocation(programID, "M");

	ProjectionMatrix = glm::perspective(45.8f, 4.f / 3.f, 0.1f, 200.0f);
	ViewMatrix = glm::lookAt(
			glm::vec3( 0, 0, 0.5 ), // Camera is here
			glm::vec3( -0.1, 0, 1.5), // and looks here
			//glm::vec3( -0.5, 0, 1.5), // and looks here
			glm::vec3( 0, 1.2, 0 )  // Head is up (set to 0,-1,0 to look upside-down)
		);

	ViewMatrix_second = glm::lookAt(
			glm::vec3( 0, 0, 0.5 ), // Camera is here
			glm::vec3( 0.1, 0, 1.5), // and looks here
			glm::vec3( 0, 1.2, 0 )  // Head is up (set to 0,-1,0 to look upside-down)
		);
}



