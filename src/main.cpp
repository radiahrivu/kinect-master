/*
 * main.cpp
 *
 *  Created on: Dec 6, 2010
 *      Author: papazov
 */


#include <Eigen/Core>
#include <Eigen/Dense>
#include <fstream>
#include <memory>
#include <iostream>
#include <pthread.h>
#include <vector>
#include <semaphore.h>
#include <sstream>
#include <string>
#include <unistd.h>
#include <opencv2/viz/types.hpp>
#include <opencv2/viz/widgets.hpp>
#include <opencv2/viz/viz3d.hpp>
#include <opencv2/viz/vizcore.hpp>
#include "core.hpp"

//#include <opencv2/opencv.hpp>
//#include <opencv2/videoio/videoio.hpp>
//#include <opencv2/features2d.hpp>
//#include <opencv2/imgproc.hpp>
//#include <opencv2/highgui.hpp>
 #include "opencv2/highgui/highgui.hpp"
 #include "opencv2/imgproc/imgproc.hpp"

//#include "dataDeclaration.h"
#include "PFTracker.h"
#include "ObjectDetection.h"

#include "Test.h"
#include "VTKExtra.h"
#include "ReadFile.h"
#include "WriteFile.h"
#include "DataParser.h"
#include "DataFilter.h"
#include "ActionParser.h"
#include "ActionPrediction.h"

#include "OpenGLViewer.h"
#include "util.h"

#include "TestCase.h"
//#include "festival.h"

#define X_POS_SECOND 1500   //this one determines the x location for the viewport on the top screen
#define X_POS_FIRST 0  	    //for the bottom screen
#define Y_POSE 150	    //this one determines the y location for the viewport
#define ZOOM_VALUE 3	    //this one determines the view value for the zoom



// for viewer
vec3 gPosition1;  //this one will store the current position for the tracking object
vec3 gPosition4;  //this one is for various functionality icons
vec3 gPosition3; //store the initial position of the cube depending on the location of the object 
vec3 gPosition2;
vec3 gOrientation1(0.1f,0.2f,0.0f);
quat myQuat;   //this will store the current orientation for the tracking object


int source_node=1; 
int destination_node=1;; 
//double probs[8];
float probs[8];      //to store the probability of the trajectories
bool value=false;     //to ensure that we get zoom only once at the beginning of the system

//int frames=0;   //this is for taking rgb images

// thread locks
sem_t mutex_contactdetector, mutex_tracker, mutex_viewer,
		mutex_actionrecognition, mutex_facedetector, mutex_tts;
sem_t lock_contactdetector, lock_tracker, lock_viewer, lock_actionrecognition,
		lock_facedetector;

std::string obj_model, obj_name;
std::string phrase;

cv::Mat img_rgb_global   = cv::Mat::zeros(480,640,CV_8UC3);
cv::Mat img_rgb_global_o = cv::Mat::zeros(480,640,CV_8UC3);
cv::Mat img_rgb_global_h = cv::Mat::zeros(480,640,CV_8UC3);
cv::Mat img_rgb_global_f = cv::Mat::zeros(480,640,CV_8UC3);
cv::Mat img_depth_global = cv::Mat::zeros(480,640,CV_16UC1);
cv::Mat img_cloud_global = cv::Mat::zeros(480,640,CV_32FC3);
cv::Mat img_cloud_global_c = cv::Mat::zeros(480,640,CV_32FC3);

cv::Mat mask_hand_global = cv::Mat::zeros(480,640,CV_8UC1);
cv::Mat mask_obj_global  = cv::Mat::zeros(480,640,CV_8UC1);

cv::Rect box_obj_global, box_hand_global;

bool contact_obj { false };
bool object_only { false };
int THRESHOLD = 200;

std::string PARENT		= "Data2";
std::string KB_DIR		= "kb";
std::string DATA_DIR	= "recording";
std::string EVAL		= "Scene_Moveface";
std::string RESULT		= "Result_ObjectState";

float frame_number_global { 0.0 };
cv::Vec3f single_point_face_global;




//**************************************************************************

	void thresh_callback(int, void* );

	cv::Mat src; cv::Mat src_gray;  					//For gesture recognition	
	int thresh = 100;
 	int max_thresh = 255;
 	cv::RNG rng(12345);	

//**************************************************************************






// =============================================================================
// Tracker
// =============================================================================
void* dbotthread(void* arg)
{ 

	std::string obj_name_ = obj_name;

	// Tracker initialization
	auto PFT = std::make_shared<PFTracker>();
	PFT->Build("..");

    Eigen::Vector3d p(0.0, 0.0, 0.0);
    Eigen::Quaternion<double> q(1.0, 0.0, 0.0, 0.0);

    dbot::PoseVelocityVector pose;
    pose.position() = p;
    pose.orientation().quaternion(q);

	// image variables
	cv::Mat img_rgb 		= cv::Mat::zeros(480,640,CV_8UC3);
	cv::Mat img_depth_rgb	= cv::Mat::zeros(480,640,CV_8UC3);
	cv::Mat img_depth		= cv::Mat::zeros(480,640,CV_16UC1);
	cv::Mat img_cloud		= cv::Mat::zeros(480,640,CV_32FC3);

	cv::Vec3f single_point_obj;

	int counter = 40;
	int x, y, z;

	std::string OUTPUT_RES1;
	std::string OUTPUT_RES2;
	std::string RESULT_DIR	=  PARENT + "/" + RESULT + "/" + obj_name_ + "/";
	directoryCheck(RESULT_DIR);

    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%y%m%d%H%M%S", &tstruct);
	OUTPUT_RES1 = "example_";
	OUTPUT_RES1 += buf;
	OUTPUT_RES1 += ".txt";
	OUTPUT_RES2 = "recording_";
	OUTPUT_RES2 += buf;
	OUTPUT_RES2 += ".txt";
	OUTPUT_RES1 = RESULT_DIR + OUTPUT_RES1;
	OUTPUT_RES2 = RESULT_DIR + OUTPUT_RES2;

	cv::VideoCapture kinect(CV_CAP_OPENNI2); 
	printf("Starting Kinect ...\n");

	std::ofstream myfile, write_file;
	myfile.open     (OUTPUT_RES1.c_str(), std::ios::out | std::ios::app);
	write_file.open (OUTPUT_RES2.c_str(), std::ios::out | std::ios::app);

	while(1)
	{
		sem_wait(&lock_tracker);
		sem_wait(&lock_tracker);
		sem_wait(&lock_tracker);
		sem_wait(&mutex_tracker);
		
		kinect.grab();
		kinect.retrieve(img_rgb,CV_CAP_OPENNI_BGR_IMAGE);
		kinect.retrieve(img_depth,CV_CAP_OPENNI_DEPTH_MAP);
		kinect.retrieve(img_cloud,CV_CAP_OPENNI_POINT_CLOUD_MAP);
		frame_number_global = kinect.get(CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_POS_FRAMES);
		//cout << "Frame number global: " << frame_number_global << endl

  /*double fps = kinect.get(CV_CAP_PROP_FPS);
  int num_frames = 120;
     
    // Start and end times
    time_t start, end;
     
    // Variable for storing video frames
   cv::Mat frame;
 
    cout << "Capturing " << num_frames << " frames" << endl ;
 
    // Start time
    time(&start);
     
    // Grab a few frames
    for(int i = 0; i < num_frames; i++)
    {
        kinect >> frame;
		int abcbd=1;
    }
     
    // End Time
    time(&end);
     
    // Time elapsed
    double seconds = difftime (end, start);
    cout << "Time taken : " << seconds << " seconds" << endl;
     
    // Calculate frames per second
    fps  = num_frames / seconds;
    cout << "Estimated frames per second : " << fps << endl;*/


		 // double fps = kinect.get(CV_CAP_PROP_FPS);
    // If you do not care about backward compatibility
    // You can use the following instead for OpenCV 3
     //double fps = video.get(CAP_PROP_FPS);
    //cout << "Frames per second using kinect.get(CV_CAP_PROP_FPS) : " << fps << endl;


		

		img_rgb_global		= img_rgb.clone();
		img_rgb_global_o	= img_rgb.clone();
		img_rgb_global_h	= img_rgb.clone();
		img_rgb_global_f	= img_rgb.clone();
		img_depth_global	= img_depth.clone();
		img_cloud_global	= img_cloud.clone();
		img_cloud_global_c	= img_cloud.clone();

		/*std::cout<<"frame number"<<frames<<std::endl;

		cv::Mat try_roi_show(img_rgb,cv::Rect(150,230,200,160));
		imshow("try taking picture", try_roi_show);
		//cv::viz::writeCloud("shapes.ply", img_cloud, cv::noArray(), cv::noArray(), false);


		std::vector<int> compression_params;
    		compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    		compression_params.push_back(9);
		frames++;
		if(frames==10)
		{
			cv::Mat try_roi(img_rgb,cv::Rect(150,230,200,160));
			cv::Mat try_cloud(img_cloud,cv::Rect(150,230,200,160));			
			cv::viz::writeCloud("stackcylinder.ply", img_cloud, img_rgb_global, cv::noArray(), false);
			
			imwrite("stackcylinder.png",img_rgb,compression_params);
		}*/

		sem_post(&lock_facedetector);

		if(0)
		{
			cv::Mat depth_image = cv::Mat::zeros(480,640,CV_8UC3);
			depthImaging(img_depth_rgb, img_depth);
			imshow("depth",img_depth_rgb);
			imshow("rgb",img_rgb);
			cvWaitKey(1);
		}

		if(counter > 0)
		{
			counter--;
			//std::cout<<"counter value :"<<counter<<std::endl;
			//imshow("point cloud", img_rgb);
			if(counter == 0)
			{

				#ifdef NEVER
				p[0] = single_point_obj[0];
				p[1] = single_point_obj[1];
				p[2] = single_point_obj[2];
				#endif

				std::vector<std::vector<std::string> > data_full;
				readFile(
						std::string(
								"../config/pose_cache_" +
								obj_model + ".txt").c_str(),
								data_full, ' ');
					//std::string("../config/location_cup.txt").c_str(),data_full,' ');

				p[0]  = std::stof(data_full[0][0]);
				p[1]  = std::stof(data_full[0][1]);
				p[2]  = std::stof(data_full[0][2]);
				//p[0]= -0.42749;
				//p[1]= 0.307435f;
				//p[2]= 1.10724f;
				q.w() = std::stof(data_full[0][3]);
				q.x() = std::stof(data_full[0][4]);
				q.y() = std::stof(data_full[0][5]);
				q.z() = std::stof(data_full[0][6]);

				//std::cout<<"initial position: "<<p[0]<<" "<<p[1]<<" "<<p[2]<<std::endl;
			
				pose.position() = p;
   				pose.orientation().quaternion(q);

				auto initial_poses = PFT->InitialPoses();
				initial_poses[0].component(0) = pose;
				PFT->InitialPoses(initial_poses);

				PFT->Initialize();
				printf("===================================================\n");

					
			}

			sem_post(&lock_tracker);
			sem_post(&lock_tracker);
			
		}
		else
		{
			auto poses = PFT->Track(img_depth);
			auto row =
					(PFT->ObjectIndex() / (640/PFT->DownsamplingFactor())) *
							PFT->DownsamplingFactor();
			auto col =
					(PFT->ObjectIndex() % (640/PFT->DownsamplingFactor())) *
							PFT->DownsamplingFactor();

			auto pp = poses.component(0).position();
			auto rm = poses.component(0).orientation().rotation_matrix();

			auto qu = poses.component(0).orientation().quaternion();

			auto ea = PFT->EulerAngle(rm);

			myQuat.w = qu.w();
			myQuat.x = qu.x();
			myQuat.y = qu.y();
			myQuat.z = qu.z();
			gPosition1.x = -(float)pp[0]*1.f;
			gPosition1.y = -(float)pp[1]*1.f;
			gPosition1.z =  (float)pp[2]*1.f;
			// The window of search for hand contact is fixed manually.
			if (obj_name_ == "CUP")
			{
				z = 80;
				if(row>360) 	 y = 20;
				else 			 y = 15;
				if(col>480) 	 x = 18;
				else if(col<160) x = 8;
				else 			 x = 13;
			}
			else if (obj_name_ == "SPG")
			{
				z = 80;
				if(row>360) 	 y = 20;
				else 			 y = 15;
				if(col>480) 	 x = 15;
				else if(col<160) x = 5;
				else 			 x = 10;
			}
			else if (obj_name_ == "APP")
			{
				z = 80;
				if(row>360) 	 y = 20;
				else 			 y = 15;
				if(col>480) 	 x = 18;
				else if(col<160) x = 8;
				else 			 x = 13;
			}

			box_obj_global.x =
					col-(PFT->DownsamplingFactor()*x) < 0 ?
							0: col-(PFT->DownsamplingFactor()*x);
			box_obj_global.y =
					row-(PFT->DownsamplingFactor()*y) < 0 ?
							0: row-(PFT->DownsamplingFactor()*y);
			box_obj_global.width = box_obj_global.height = z;




			// [WRITE] ********************************************************************* // commenting out the writing portion
			/*if(frame_number_global>50.0)
			{
				myfile	<< pp[0]	<< " " << pp[1]	  << " " << pp[2]	<< " "
					 	<< rm(0,0)	<< " " << rm(0,1) << " " << rm(0,2) << " "
					 	<< rm(1,0)	<< " " << rm(1,1) << " " << rm(1,2) << " "
					 	<< rm(2,0)	<< " " << rm(2,1) << " " << rm(2,2) << " "
					 	<< ea[0]	<< " " << ea[1]	  << " " << ea[2] 	<< " "
					 	<< qu.w()	<< " " << qu.x()  << " " << qu.y() 	<< " " << qu.z() << " "
					 	<< contact_obj	<< "\n";

				write_file 	<< frame_number_global << ","
							<< contact_obj << ","
							<< pp[0]   << ","
							<< pp[1]   << ","
							<< pp[2]   << ","
							<< single_point_face_global[0] << ","
							<< single_point_face_global[1] << ","
							<< single_point_face_global[2] << ","
							<< rm(0,0) << "," << rm(0,1) << "," << rm(0,2) << ","
							<< rm(1,0) << "," << rm(1,1) << "," << rm(1,2) << ","
							<< rm(2,0) << "," << rm(2,1) << "," << rm(2,2) << ","
							<< ea[0]   << "," << ea[1]   << "," << ea[2]   << ","
							<< qu.w()  << "," << qu.x()  << "," << qu.y()  << "," << qu.z()
							<< "\n";
			}*/
			// ********************************************************************* [WRITE]

			sem_post(&lock_contactdetector);
			sem_post(&lock_actionrecognition);
		}

		sem_post(&mutex_tracker);
		sem_post(&lock_viewer);
	}



	return 0;
}

// =============================================================================
// VIEWER
// =============================================================================
void resetGlmVec3(std::vector<glm::vec3> &alpha_values)  //function to reset the alpha channel 
{
	for(int i=0 ; i<alpha_values.size() ; i++)
		alpha_values[i][0]=0;
	
}

void* openglthread(void* arg)
{ 
	OpenGLViewer viewer;
 	GLFWwindow* window;
	GLFWwindow* window_second;
	
 	GLuint VertexArrayID, vertexbuffer, colorbuffer;
	GLuint vertexbufferAxes, colorbufferAxes;
	GLuint colorbuffer1;                                                    //for generating color for the location points
	GLuint circlebuffer;     						//for generating the location points 
	GLuint trajectorybuffer;						//for generatiing the trajectory points
	GLuint trajectory_color_buffer;						//for generating the color of the trajectory points
	GLuint grasp;								//for generating the grasping points
	GLuint alpha_buffer;							//for generating the alpha value of the trajectory
	GLuint cube_alpha;							//for generating the alpha value of the cube
	GLuint cube_buffer;							//for generating the cube around the object
	GLuint cube_color;							//for generating the color of the cube
	GLuint Texture;								//to load the bitmap for the banner
	GLuint Texture_second;							//to load the bitmap for the banner
	GLuint Texture_third;							//to load the bitmap for the banner
	GLuint functionality_buffer;						//to load the cube to represent functionality
	GLuint functionality_uv_buffer;						//to load the uv to represent functionality
	GLuint zoom_line_buffer;						//to load the lines for the zooming
	GLuint programID, MatrixID, ViewMatrixID, ModelMatrixID,TextureID;
	
	int lookup[8][8];							//lookup table to determine which path currently to show in terms of alpha color
	int lookup_ellipse [8][8];						//lookup table to determine which path currently to show in terms of ellipse
	bool changed = true;	

	int dst_node[8];							//array for destination node
	
	int size_array=0; 						//this will define the number of paths for each object
	glm::mat4 ViewMatrix,ViewMatrix_second, ProjectionMatrix, ModelMatrix, MVP;            //for the viewing of the objects
	
	std::vector<glm::vec3>location_points;    					//for storing the location points
	std::vector<glm::vec3>vertices;
	std::vector<glm::vec3>trajectory_points; 			//will store the points 
	std::vector<glm::vec3>trajectory_color; 			//for storing the initial trajectory color
	std::vector<glm::vec3>store; 					//this will store the trajectory points
	std::vector<glm::vec3>alpha_value;				//will store the alpha channel
	std::vector<glm::vec3>ellipse_loc;				//will store the ellipse locations
	std::vector<glm::vec3>grasp_points;				//will store the grasp_points
	std::vector<glm::vec3>cube_points;				//will store the vertices of the cube
	std::vector<glm::vec3>cube_alpha_points;			//will store the alpha value of the cube
	std::vector<int> temp_value;	
	std::vector<glm::vec3>ellipse_scale;				//will contain the x and y values from the sector map	

	std::vector<std::vector<std::string> > cube_location;		//to read the location for the cube

readFile(
					//std::string("../config/location_cup.txt").c_str(),cube_location,' ');
					std::string(
								"../config/pose_cache_" +
								obj_model + ".txt").c_str(),
								cube_location, ' ');

				float r  = std::stof(cube_location[0][0]);
				float s  = std::stof(cube_location[0][1]);
				float t  = std::stof(cube_location[0][2]);
					gPosition3.x = -(float)r*1.f;
					//std::cout<<"value of gposition3.x   "<<gPosition3.x<<std::endl;
					gPosition3.y = -(float)s*1.f;
					gPosition3.z =  (float)t*1.f;



	//char* source_window = "Source";
   	//cv::namedWindow( source_window, CV_WINDOW_AUTOSIZE );
   	//cv::imshow( source_window, src );
	
	// Create Window
	if (1)
	{	
		// Initialise GLFW
		if( !glfwInit() )
		{
			fprintf( stderr, "Failed to initialize GLFW\n" );
			getchar();
			return 0;
		}

		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_DECORATED,0);  //trying to fix the dimension without border
		//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); 
		//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		// Open a window and create its OpenGL context
		window = glfwCreateWindow(2800, 1050, "viewer", NULL, NULL);
		//window_second = glfwCreateWindow(2800, 1050, "viewer_second", NULL, NULL);
		
		if( window == NULL ){
			fprintf(
					stderr,
					"Failed to open GLFW window. ");
			fprintf(
					stderr,
					"If you have an Intel GPU, they are not 3.3 compatible. ");
			fprintf(
					stderr,
					"Try the 2.1 version of the tutorials.\n" );
			getchar();
			glfwTerminate();
			return 0;
		}

		glfwSetWindowPos(window,0,0);
		//glfwSetWindowPos(window_second,1400,0);
		
		//GLint *see;
		
		
		//glGetIntegerv(GL_MAX_VIEWPORT_DIMS,see);

		//std::cout<<"glviewport : "<<see<<std::endl;

		glfwMakeContextCurrent(window);
		//glfwMakeContextCurrent(window_extra);

		// Ensure we can capture the escape key being pressed below
		glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
		
	
		// Enable depth test
		glEnable(GL_DEPTH_TEST);cv::Mat roi(img_rgb_global,cv::Rect(90,50,150,160));

		// Accept fragment if it closer to the camera than the former one
		glDepthFunc(GL_LESS); 

		// Dark blue background
		glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	}

	viewer.Initialize(VertexArrayID, vertexbuffer, colorbuffer,
		vertexbufferAxes, colorbufferAxes, programID,
		MatrixID, ViewMatrixID, ModelMatrixID,TextureID,colorbuffer1,circlebuffer,trajectorybuffer,trajectory_color_buffer,alpha_buffer,grasp,cube_buffer,cube_color,cube_alpha,Texture,Texture_second,Texture_third,functionality_buffer,functionality_uv_buffer,zoom_line_buffer,dst_node, ViewMatrix,ViewMatrix_second, ProjectionMatrix,
		vertices,location_points,trajectory_points,trajectory_color,store,temp_value,alpha_value,grasp_points,cube_points,cube_alpha_points,size_array,ellipse_loc,ellipse_scale,lookup,lookup_ellipse,obj_model.c_str());

	
	//std::cout<<"size of size array :"<<size_array<<std::endl;
	//std::cout<<"size of temp_value :"<<temp_value.size()<<std::endl; 

	GLuint vertexbufferscene, colorbufferscene;			// for viewing the point cloud
	glGenBuffers(1, &vertexbufferscene);
	glGenBuffers(1, &colorbufferscene);


		GLint firstA[size_array]; 
		GLint countA[size_array]; 					//Points to an array of the number of indices to be rendered.
		int count_value=temp_value[1]-temp_value[0];
		
                for(int i=0;i<temp_value.size();i++)
		{
			firstA[i]=temp_value[i];
			countA[i]=count_value;

		}
			
	 // Check if the ESC key was pressed or the window was closed
	//while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
	//	   glfwWindowShouldClose(window) == 0 )
	while(1)	
	{
		sem_wait(&lock_viewer);
		sem_wait(&mutex_viewer);


		// Clear the screen. 
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);
		//glEnable(GL_PROGRAM_POINT_SIZE);
		glEnable(GL_LINE_SMOOTH);
		glPointSize(4.0f);
		
		

//if (!object_only)

	



//{
		// [SCENE] *************************************************************
		std::vector<glm::vec3> vertices2, verticesC;
		for(int i=0;i<img_cloud_global.size().height;i++)
		{
			for(int ii=0;ii<img_cloud_global.size().width;ii++)
			{
				glm::vec3 vertice(
						-img_cloud_global.at<cv::Vec3f>(i,ii)[0],
						 img_cloud_global.at<cv::Vec3f>(i,ii)[1],
						 img_cloud_global.at<cv::Vec3f>(i,ii)[2]);
				vertices2.push_back(vertice);
			}
		}
		for(int i=0;i<img_rgb_global.size().height;i++)
		{
			for(int ii=0;ii<img_rgb_global.size().width;ii++)
			{
				glm::vec3 vertice(
						(float)img_rgb_global.at<cv::Vec3b>(i,ii)[2]/255.0,
						(float)img_rgb_global.at<cv::Vec3b>(i,ii)[1]/255.0,
						(float)img_rgb_global.at<cv::Vec3b>(i,ii)[0]/255.0);
				verticesC.push_back(vertice);
			}
		}
		//frames++;
		//cv::Mat twoviewcloud;//=cv::Mat::zeros(480,640,CV_32FC3);
		//if(frames==13)
		//twoviewcloud=img_cloud_global.clone();
		//std::cout<<"frames number: "<<frames<<std::endl;
		//cv::Mat img_cloud_global = cv::Mat::zeros(480,640,CV_32FC3);
		//cv::viz::writeCloud("trial.ply", img_cloud_global, cv::noArray(), cv::noArray(), false);
		//cv::viz::writeCloud("trial.pcd", img_cloud_global, cv::noArray(), cv::noArray(), false);

		//FOR THE object
		glBindBuffer(GL_ARRAY_BUFFER, vertexbufferscene);
		glBufferData(GL_ARRAY_BUFFER, vertices2.size() * sizeof(glm::vec3),
				&vertices2[0], GL_STATIC_DRAW);

		//For the Color
		glBindBuffer(GL_ARRAY_BUFFER, colorbufferscene);
		glBufferData(GL_ARRAY_BUFFER, verticesC.size() * sizeof(glm::vec3),
				&verticesC[0], GL_STATIC_DRAW);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbufferscene);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbufferscene);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		ModelMatrix = 
				scale(mat4(), vec3(1.f, 1.f, 1.f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		//glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		//glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
		
		// Draw the triangle !
	
		 
		glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
		glDrawArrays(GL_POINTS, 0, vertices2.size() );
	
		ModelMatrix = 
				scale(mat4(), vec3(-1.f, 1.f, 1.f));
		MVP = ProjectionMatrix * ViewMatrix_second * ModelMatrix;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_POINTS, 0, vertices2.size() );

		// ************************************************************* [SCENE]
//}
		

		//* [GESTURE RECOGNITION]******************************************************************
	/*	cv::Mat temp(img_rgb_global,cv::Rect(90,50,150,160));	 //ROI for hand detection
		//src=img_rgb_global.clone();
		src=temp;
		cv::cvtColor( src, src_gray, CV_BGR2GRAY );		//converting from one color space to another
   		cv::blur( src_gray, src_gray, cv::Size(3,3) );           //filtering the images

		std::string source_window = "Source";
   		//cv::namedWindow( source_window, CV_WINDOW_NORMAL);
   		//imshow( source_window, src );

		//cv::createTrackbar( " Threshold:", "Source", &thresh, max_thresh, thresh_callback );		
		thresh_callback( 0, 0 );

	*/
		//*****************************************************************************************

		// [OBJ WITH ZOOM FUNCTION ] ***************************************************************

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		ModelMatrix =
				glm::translate(mat4(), gPosition1) * 
				glm::toMat4(myQuat) *
				scale(mat4(), vec3(0.8f, 0.8f, 0.8f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		//glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		

		// Draw the object
		//glDrawArrays(GL_TRIANGLES, 0, vertices.size() );


		cv::Mat fullImageHSV;
		cv::Mat roi(img_rgb_global,cv::Rect(90,50,150,160));
		cv::Mat red_hue_image;
		//cv::imshow("Hand Detection ROI", roi);
		//cv::waitKey(25);

		cvtColor(roi,fullImageHSV,CV_BGR2HSV); 		//converts to HSV color space
		cv::Mat low_red_hue; //selecting an upper range for the chosen color
		cv::Mat high_red_hue;//selecting a lower range for the chosen color
		inRange(fullImageHSV,cv::Scalar(0,100,100),cv::Scalar(10,255,255),low_red_hue);
		inRange(fullImageHSV,cv::Scalar(160,100,100),cv::Scalar(179,255,255),high_red_hue);
		

		//cv::inRange(fullImageHSV, cv::Scalar(130, 10,75), cv::Scalar(160,40,130), fullImageHSV);
		
		addWeighted(low_red_hue,1.0,high_red_hue,1.0,0.0,red_hue_image);

		cv::SimpleBlobDetector::Params params;
		params.minThreshold=10;
		params.maxThreshold=200;
		params.filterByArea=true;
		params.minArea=0;
		params.maxArea=1000;
		params.filterByColor=true;
		params.blobColor=255;
		
		
		cv::Ptr<cv::SimpleBlobDetector>detector = cv::SimpleBlobDetector::create(params);
		std::vector<cv::KeyPoint> keypoints;				//will store the number 
		detector->detect(red_hue_image,keypoints);	
		//detector->detect(fullImageHSV,keypoints);			//will detect color		
		cv::Mat im_with_keypoints;					//will store the output image
		drawKeypoints(red_hue_image,keypoints,im_with_keypoints,cv::Scalar(255,0,0),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);  //drawing the circles
		//imshow("keypoints ", im_with_keypoints);  
		//std::cout<<" keypoints size :	 "<<keypoints.size()<<std::endl;
		//int seconds=clock()/1000;
		//int seconds_extra=seconds;
		if(keypoints.size() >= ZOOM_VALUE && value == false)
			{
				//std::cout<<"hand detected: "<<std::endl;
				//std::cout<<"value of keypoints size 2: "<<keypoints.size()<<std::endl;
				// int milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;
				//int seconds=clock()/1000;
				//int flag=seconds+15000;
				//std::cout<<"seconds :"<<seconds<<std::endl;
				//while(seconds_extra<flag)
				
				//for(int a=0;a<25000;a++)
				
				{	
				
										
					glm::mat4 rotMatrix = eulerAngleYXZ(-0.9, 1.5, -0.9);  //the orientation of the zoomed object,hard-coded,bad

					//ModelMatrix=glm::translate(mat4(), vec3(-0.5,0.5,1.5)) *rotMatrix *scale(mat4(), vec3(2.8f, 2.8f, 2.8f)); 
					ModelMatrix=glm::translate(mat4(), vec3(-0.5,0.5,1.5)) *rotMatrix *scale(mat4(), vec3(2.8f, 2.8f, 2.8f)); 
					MVP=ProjectionMatrix*ViewMatrix*ModelMatrix;
					glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

					glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
					glDrawArrays(GL_TRIANGLES, 0, vertices.size() );


					ModelMatrix=glm::translate(mat4(), vec3(0.5,0.5,1.5)) *rotMatrix *scale(mat4(), vec3(-2.8f, 2.8f, 2.8f)); 
					MVP=ProjectionMatrix*ViewMatrix_second*ModelMatrix;
					glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

					glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
					glDrawArrays(GL_TRIANGLES,0,vertices.size());
					
					//value=true;
					//seconds_extra++;
	

				}
				//value=true;
			}
		else 
		{
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
			glDrawArrays(GL_TRIANGLES, 0, vertices.size() );


			gPosition2.x= gPosition1.x* -1.0f;

			//std::cout<<"gPOsition1.x "<<gPosition1.x<<std::endl;
			//std::cout<<"gPOsition2.x "<<gPosition2.x<<std::endl;
			gPosition2.y= gPosition1.y;
			gPosition2.z= gPosition1.z;
			ModelMatrix =
				glm::translate(mat4(), gPosition2) * 
				glm::toMat4(myQuat) *
				scale(mat4(), vec3(0.8f, 0.8f, 0.8f));
			MVP = ProjectionMatrix * ViewMatrix_second * ModelMatrix*scale(mat4(), vec3(-1.f, 1.f, 1.f));
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);


			glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
			glDrawArrays(GL_TRIANGLES,0,vertices.size());


			
		}
		
		// Draw the object
		/*glViewport (0, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES, 0, vertices.size() );

			gPosition2.x= gPosition1.x* -1.0f;

			//std::cout<<"gPOsition1.x "<<gPosition1.x<<std::endl;
			//std::cout<<"gPOsition2.x "<<gPosition2.x<<std::endl;
			gPosition2.y= gPosition1.y;
			gPosition2.z= gPosition1.z;
		ModelMatrix =
				glm::translate(mat4(), gPosition2) * 
				glm::toMat4(myQuat) *
				scale(mat4(), vec3(0.8f, 0.8f, 0.8f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix*scale(mat4(), vec3(-1.f, 1.f, 1.f));
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);


		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES,0,vertices.size());*/

		// ****************************************************************************************
		// [OBJ-AXIS] *****************************************************************************
		
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbufferAxes);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbufferAxes);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	
		ModelMatrix =
				glm::translate(mat4(), gPosition1) * 
				glm::toMat4(myQuat) *
				scale(mat4(), vec3(0.8f, 0.8f, 0.8f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
		glDrawArrays(GL_LINES, 0, 6*3);

		ModelMatrix =
				glm::translate(mat4(), gPosition2) * 
				glm::toMat4(myQuat) *
				scale(mat4(), vec3(0.8f, 0.8f, 0.8f));
		MVP = ProjectionMatrix * ViewMatrix_second* ModelMatrix*scale(mat4(), vec3(-1.f, 1.f, 1.f));
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_LINES,0,6*3);
		
		// *******************************************************************************************
		//[ZOOM LINES]********************************************************************************

			glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,zoom_line_buffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		ModelMatrix =
				
				scale(mat4(), vec3(0.8f, 0.8f, 0.8f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		

		// Draw the object
		//glDrawArrays(GL_LINES, 0, 4*3);
			if (keypoints.size()>=ZOOM_VALUE && value == false)
			{
				for(int a=0;a<1500;a++)
				glPointSize(8.0f);
				
				glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
				glDrawArrays(GL_LINES, 0, 4*3);

				

				ModelMatrix = scale(mat4(), vec3(-0.8f, 0.8f, 0.8f));
				MVP = ProjectionMatrix * ViewMatrix_second * ModelMatrix;

				glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

				glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
				glDrawArrays(GL_LINES, 0, 4*3);

			}
			//glPointSize(4.0f);
		//********************************************************************************************
		//[INTERACTION VOLUME ]**************************************************************************************

		
		ModelMatrix =glm::translate(mat4(), gPosition3)*scale(mat4(), vec3(2.2f, 2.1f, 1.0f)); //vec3(0.35,0.35,1.5)
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
		// use the MVP to project the pixel matrix using opencv functions.

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glBindTexture(GL_TEXTURE_2D, 0);			//to disable texture mapping
		glEnable(GL_BLEND);					//enables alpha blending
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);       //the rule for alpha blending

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,cube_buffer);
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*)0);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, cube_color);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);
		glEnableVertexAttribArray(2);
				glBindBuffer(GL_ARRAY_BUFFER,cube_alpha);
				glVertexAttribPointer(
					2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
					3,                                // size
					GL_FLOAT,                         // type
					GL_FALSE,                         // normalized?
					0,                                // stride
					(void*)0                          // array buffer offset
				);
			

		glViewport (X_POS_FIRST,Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES,0,cube_points.size());

		ModelMatrix =glm::translate(mat4(), vec3(-gPosition3.x, gPosition3.y, gPosition3.z))*scale(mat4(), vec3(2.2f, 2.1f, 1.0f)); //vec3(-0.35f,0.35f,1.5f))
		MVP = ProjectionMatrix * ViewMatrix_second * ModelMatrix*scale(mat4(), vec3(-1.f, 1.f, 1.f));
		// use the MVP to project the pixel matrix using opencv functions.

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES,0,cube_points.size());



		glDisable(GL_BLEND);
		

		//************************************************************************************************
		





		//[OBJECT SPECIFIC FUNCTIONS]*****************************************************************************
		
	/*	ModelMatrix =glm::translate(mat4(), vec3(gPosition3.x,gPosition3.y+0.05,gPosition3.z))*scale(mat4(), vec3(0.04f, 0.07f, 0.09f)); //vec3(0.3f,0.40f,1.5f)
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);


		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to use Texture Unit 0
		glUniform1i(TextureID, 0);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, functionality_buffer);
		glVertexAttribPointer(
			0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, functionality_uv_buffer);
		glVertexAttribPointer(
			3,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			2,                                // size : U+V => 2
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Draw the triangle !
		glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES, 0,6*3);


		ModelMatrix =glm::translate(mat4(), vec3(-gPosition3.x,gPosition3.y+0.05,gPosition3.z))*scale(mat4(), vec3(0.04f, 0.07f, 0.09f)); //vec3(-0.3f,0.40f,1.5f)
		MVP = ProjectionMatrix * ViewMatrix_second* ModelMatrix* scale(mat4(), vec3(-1.f, 1.f, 1.f));

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES, 0,6*3);*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//std::cout<<"source node: "<<source_node<<endl;


		if(source_node == 1)
		{
			gPosition4.x = 0.326072;
			gPosition4.y = 0.3481411;
			gPosition4.z = 1.4876;
			
			
			glBindTexture(GL_TEXTURE_2D, Texture);
			//std::cout<<"printing"<<source_node<<endl;
		}

		if(source_node == 2)
		{
			gPosition4.x = 0.24433;
			gPosition4.y = -0.205084;
			gPosition4.z = 1.5241;

			glBindTexture(GL_TEXTURE_2D, Texture_second);
		}

		if(source_node == 4)
		{
			gPosition4.x = 0.227728;
			gPosition4.y = -0.345997;
			gPosition4.z = 1.0278;

			glBindTexture(GL_TEXTURE_2D, Texture_third);
		}
	
		if(source_node == 6)
		{
			gPosition4.x = 0.0053599;
			gPosition4.y = 0.25645;
			gPosition4.z = 1.28532;

			glBindTexture(GL_TEXTURE_2D, Texture);
		}
		
		if(source_node == 7)
		{
			gPosition4.x = -0.298511;
			gPosition4.y = -0.371976;
			gPosition4.z = 1.80589;

			glBindTexture(GL_TEXTURE_2D, Texture_third);
		}



			glActiveTexture(GL_TEXTURE0);
			//glBindTexture(GL_TEXTURE_2D, Texture);
			// Set our "myTextureSampler" sampler to use Texture Unit 0
			glUniform1i(TextureID, 0);



		ModelMatrix =glm::translate(mat4(), vec3(gPosition4.x,gPosition4.y+0.05,gPosition4.z))*scale(mat4(), vec3(0.04f, 0.07f, 0.09f)); 
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);


		
		

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, functionality_buffer);
		glVertexAttribPointer(
			0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

			/*glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer1);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);*/

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, functionality_uv_buffer);
		glVertexAttribPointer(
			3,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			2,                                // size : U+V => 2
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Draw the triangle !
		glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES, 0,6*3);


		ModelMatrix =glm::translate(mat4(), vec3(-gPosition4.x,gPosition4.y+0.05,gPosition4.z))*scale(mat4(), vec3(0.04f, 0.07f, 0.09f)); 
		MVP = ProjectionMatrix * ViewMatrix_second* ModelMatrix* scale(mat4(), vec3(-1.f, 1.f, 1.f));

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_TRIANGLES, 0,6*3);
                

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




		//********************************************************************************************

		//[LOCATION POINTS]***************************************************************************

		ModelMatrix =scale(mat4(), vec3(1.0f, 1.0f, 1.0f));// glm::translate(mat4(), gPosition2)*scale(mat4(), vec3(1.0f, 1.0f, 1.0f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,circlebuffer);
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*)0);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer1);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);
		
		glViewport (X_POS_FIRST,Y_POSE, 1400, 1050);
		glDrawArrays(GL_POINTS,0,location_points.size());

		
		ModelMatrix =scale(mat4(), vec3(-1.0f, 1.0f, 1.0f));
		MVP = ProjectionMatrix * ViewMatrix_second * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		
		glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
		glDrawArrays(GL_POINTS,0,location_points.size());


		//*************************************************************************************************
	

		//[GRASPING_POINTS]********************************************************************************
		//ModelMatrix= scale(mat4(), vec3(2.8f, 2.8f, 2.8f)); 
		ModelMatrix =scale(mat4(), vec3(-1.0f, 1.0f, 1.0f));
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		//glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glDisable(GL_DEPTH_TEST);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,grasp);
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*)0);
		
		if (keypoints.size()>=ZOOM_VALUE && value == false)
		{

			glViewport (X_POS_SECOND,Y_POSE, 1400, 1050);
			glDrawArrays(GL_POINTS,0,grasp_points.size());

			ModelMatrix =glm::translate(mat4(), vec3(0.35f,0.35f,1.5f))* scale(mat4(), vec3(2.2f, 2.1f, 1.0f));
			MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

			glViewport (X_POS_FIRST,Y_POSE, 1400, 1050);
			glDrawArrays(GL_POINTS,0,grasp_points.size());
			value=true;

		}
		
		glEnable(GL_DEPTH_TEST);





		//***************************************************************************************************

		//[ELLIPSES TO GENERATE SECTOR MAP]***********************************************************
			GLUquadricObj *mySphere;  // creating quadratic object to draw sphere
			mySphere= gluNewQuadric();
			GLUquadricObj *myCircle;
			myCircle=gluNewQuadric();


			//GLdouble radius= 0.01;
			GLdouble radius=0.02;
			GLint slices=12;
			GLint stacks=12;
	

			gluQuadricDrawStyle(mySphere, GLU_FILL);
			gluQuadricNormals(mySphere,GLU_FLAT);
			gluQuadricTexture(mySphere, GL_TRUE);

			//glm::mat4 TranslationMatrix = translate(mat4(),vec3(0.326072,0.348141,1.4876));
			glm::mat4 ScalingMatrix=scale(mat4(), vec3(1.0f, 1.0f, 1.f));
			vec3 gPositionEllipse;
			
			
			if(contact_obj)  // this loop insures that only when there is contact, the ellipses on the specific path will be drawn.
			{
				int index= lookup_ellipse[source_node][destination_node];
				//std::cout<<"value of index: "<<index<<std::endl;
				for( int i=index;i<index+3;i++	)
				{
					glm::mat4 TranslationMatrix = translate(mat4(), ellipse_loc[i]);
					glm::mat4 ScalingMatrix=scale(mat4(), vec3(0.8f, 1.2f, 1.0f));
					//glm::mat4 ScalingMatrix=scale(mat4(),ellipse_scale[i]);
					
					
					ModelMatrix= TranslationMatrix*ScalingMatrix;
					MVP= ProjectionMatrix*ViewMatrix*ModelMatrix;

        				glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
					glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
					gluSphere(mySphere, radius,slices,stacks); //first sphere
					
					gPositionEllipse.x= ellipse_loc[i].x* -1.0f;  //this is for the 3d projection, so that negative kore kora jai
					gPositionEllipse.y= ellipse_loc[i].y;
					gPositionEllipse.z= ellipse_loc[i].z;

					TranslationMatrix= translate(mat4(),gPositionEllipse);
					ModelMatrix= TranslationMatrix * ScalingMatrix * scale(mat4(), vec3(-1.f, 1.f, 1.f));
					MVP= ProjectionMatrix*ViewMatrix_second*ModelMatrix;

        				glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
					
					glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
					gluSphere(mySphere, radius,slices,stacks);




				}
			
			}


		//****************************************************************
		//[POSSIBLE TRAJECTORIES]***************************************************************
		
			
		
			//if (!contact_obj && changed)
			//	resetGlmVec3(alpha_value);
			if (!contact_obj )
				resetGlmVec3(alpha_value);
			
			if(source_node != -1 && destination_node != -1 && contact_obj)
			{
				/*int val=probs[destination_node];
				int index= lookup[source_node][destination_node];
				
				//std::cout<<"index "<<index<<"\tval : "<<val<<"\tsource: "<<source_node<<"\tdestination: "<<destination_node<<std::endl;
				if (contact_obj)
				{
					for(int x=index;x<index+100;x++)
					{				
						alpha_value[x][0]=val;
					}
				}
				//int ex= lookup[6][7];
				//cout<<"index value is "<<ex<<endl;
				//cout<<"index value with destination "<<destination_node<<" is : "<<index<<endl;*/
				
				//eikhane theke ami  kortesi for multiple path*********************************************************	
				//int index= lookup[source_node][destination_node];
				//cout<<"destination node"<<destination_node<<endl;


				float vals[8], index_value[8];
				for(int i=0;i<8;i++)
					{
						vals[i]= probs[i];
						index_value[i]=lookup[source_node][i];
						//cout<<"value at "<<i<<" is "<<index_value[i]<<" and the source node is "<<source_node<<endl;		
					}

		
				for(int i=0;i<8;i++)
				{
						
					if (vals[i]>0)
					{
						//std::cout<<vals[i]<<endl;
						int index=index_value[i];
						for(int x=index;x<index+100;x++)
							{				
                                                                      if (vals[i]>=0.5)																
									{alpha_value[x][0]= 1.0;}
									else
									{alpha_value[x][0]= 0.6;}
							}
						
					}	
					//cout<<"value at 2 3 is :"<<lookup[2][3]<<endl;	
				}
			

			}
			
			
			ModelMatrix=scale(mat4(),vec3(1.0f, 1.0f, 1.0f)); 
			glBindBuffer(GL_ARRAY_BUFFER,alpha_buffer);
			glBufferData(GL_ARRAY_BUFFER,alpha_value.size()*sizeof(glm::vec3),&alpha_value[0],GL_DYNAMIC_DRAW);
		
			MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

			glEnable(GL_BLEND);					//enables alpha blending
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);	//the equation through which blending value of the pixel will be decided


				glEnableVertexAttribArray(0);
				glBindBuffer(GL_ARRAY_BUFFER, trajectorybuffer);
				glVertexAttribPointer(
					0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
					3,                  // size
					GL_FLOAT,           // type
					GL_FALSE,           // normalized?
					0,                  // stride
					(void*)0            // array buffer offset
				);

				glEnableVertexAttribArray(1);
				glBindBuffer(GL_ARRAY_BUFFER, trajectory_color_buffer);
				glVertexAttribPointer(
					1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
					3,                                // size
					GL_FLOAT,                         // type
					GL_FALSE,                         // normalized?
					0,                                // stride
					(void*)0                          // array buffer offset
				);
				glEnableVertexAttribArray(2);
				glBindBuffer(GL_ARRAY_BUFFER,alpha_buffer);
				glVertexAttribPointer(
					2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
					3,                                // size
					GL_FLOAT,                         // type
					GL_FALSE,                         // normalized?
					0,                                // stride
					(void*)0                          // array buffer offset
				);
			
				
			

			glLineWidth(5);
			if (contact_obj == true)
			
			{	glViewport (X_POS_FIRST, Y_POSE, 1400, 1050);
				glMultiDrawArrays(GL_LINE_STRIP, firstA, countA, size_array);// multidraw array helps to draw the different paths separately in one call
				ModelMatrix=scale(mat4(),vec3(-1.0f, 1.0f, 1.0f)); 
				MVP = ProjectionMatrix * ViewMatrix_second * ModelMatrix;
				glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

				glViewport (X_POS_SECOND, Y_POSE, 1400, 1050);
				glMultiDrawArrays(GL_LINE_STRIP, firstA, countA, size_array);
			}
				glDisable(GL_BLEND);
			








		//**************************************************************************************

		//glDisable(GL_TEXTURE_2D); 		
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
	
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

		sem_post(&mutex_viewer);
		sem_post(&lock_tracker);
	}
	return 0;
}

// ============================================================================
// OBJECT DETECTOR
// ============================================================================
/*
void* objectDetector(void* arg)
{
	int hs[4]; // hue max/min, sat max/min
	hs[0] = 98; hs[1] = 90; hs[2] = 230; hs[3] = 140; // yellow sponge
	hs[0] = 27; hs[1] =  0; hs[2] = 186; hs[3] = 105; // blue cup
	hs[0] = 18; hs[1] =  0; hs[2] = 122; hs[3] =  59; // blue cup
	hs[0] = 90; hs[1] = 72; hs[2] = 179; hs[3] = 84; // green cup

	while(true)
	{
		sem_wait(&lock3);
		sem_wait(&mutex3);

		segmentHSV(img_rgb_global_o, hs, mask_obj_global, box_obj_global);

		if(0)
		{
			cv::Mat rgb_tmp = cv::Mat::zeros(480,640, CV_8UC3);
			img_rgb_global_o.copyTo(rgb_tmp, mask_obj_global);
			cv::imshow("rgb_o",rgb_tmp);
			cvWaitKey(1);
		}

		sem_post(&mutex3);
		sem_post(&lock1);
		sem_post(&lock5);
	}

	return 0;
}
*/

// ============================================================================
// HAND DETECTOR
// ============================================================================
/*
void* handDetector(void* arg)
{ 
	int hs[4]; // hue max/min, sat max/min
	hs[0] = 107; hs[1] = 90; hs[2] = 107; hs[3] = 66;
	hs[0] = 125; hs[1] = 98; hs[2] = 140; hs[3] = 77;
	hs[0] = 107; hs[1] = 98; hs[2] = 140; hs[3] = 77;

	cv::Mat img_no_head = cv::Mat::zeros(480,640,CV_8UC3);

	while(true)
	{
		sem_wait(&lock4);
		sem_wait(&mutex4);

      	//rgb_global3.clone().rowRange(200,480).copyTo(img_no_head.rowRange(200,480));
		segmentHSV(img_rgb_global_h, hs, mask_hand_global, box_hand_global);

		if(0)
		{
			cv::Mat rgb_tmp = cv::Mat::zeros(480,640, CV_8UC3);
			img_rgb_global_h.copyTo(rgb_tmp, mask_hand_global);
			cv::imshow("rgb_h",rgb_tmp);
			cvWaitKey(1);
		}

		sem_post(&mutex4);
		sem_post(&lock1);
		sem_post(&lock5);
	}

	return 0;
}
*/
// ============================================================================
// CONTACT DETECTOR
// ============================================================================
void* contactDetector(void* arg)
{
	// checks for hand presence in the box surrounding the object.
	while(1)
	{
		sem_wait(&lock_contactdetector);
		sem_wait(&mutex_contactdetector);
		cv::Mat img_rgb_tmp = cv::Mat::zeros(480,640,CV_8UC3);
		img_rgb_global_o(box_obj_global).copyTo(img_rgb_tmp); // reducing the search area
		contact_obj = contactCheckBox(img_rgb_tmp, "rgb_c", THRESHOLD, false);  

		//std::cout<<"contact value : "<<contact_obj<<std::endl;
		
		sem_post(&mutex_contactdetector);
		sem_post(&lock_tracker);
	}

/*
	cv::Mat img_depth_def, mask_obj_def, img_sub, cloud_mask,cloud_mask2;
	double contact_val;
	bool flag_contact_init	= true;
	bool flag_contact_obj	= false;
	int c = 0;
	while(true)
	{
		sem_wait(&lock5);
		sem_wait(&lock5);
		sem_wait(&mutex5);

		// [DEFAULT SCENE]*****************************************************
		if(flag_contact_init)
		{
			mask_obj_def = mask_obj_global.clone();
			img_depth_global.copyTo(img_depth_def,mask_obj_def);
			if(box_obj_global.x > 0 && box_obj_global.y > 0) 
				flag_contact_init = false;
		}
		// *****************************************************[DEFAULT SCENE]

		// [OBJECT CONTACT]****************************************************
		if(contactCheck(mask_hand_global, mask_obj_global,
						box_hand_global, box_obj_global))
		{
			if(!flag_contact_obj)
			{
				img_depth_global.copyTo(img_sub,mask_obj_def);			
				absdiff(img_depth_def, img_sub, img_sub);
				contact_val = sum(img_sub)[0] / sum(mask_obj_def)[0];

				if(contact_val > 0 && contact_val < 500)
				{
					contact_obj 		= true;
					flag_contact_obj 	= true;
				}
				else contact_obj = false;
			}
			else contact_obj = true;     
		}
		else
		{
			flag_contact_init 	= true;
			flag_contact_obj 	= false;
			contact_obj 		= false;
			contact_val			= 0.0;
		}
		// face prevention
		if(box_obj_global.tl().x > 320) 
		{
			flag_contact_init	= false;
			flag_contact_obj	= true;
			contact_obj 		= true;
			contact_val			= -1.0;
		} 
		// ****************************************************[OBJECT CONTACT]

		sem_post(&mutex5);
		sem_post(&lock1);
	}
*/
	return 0;
}

// ============================================================================
// FACE DETECTOR
// ============================================================================
void* faceDetector(void* arg)
{
	//Load the cascade for face detector
	std::string face_cascade_name = "../cascade/lbpcascade_frontalface.xml";
	cv::CascadeClassifier face_cascade;
	if(!face_cascade.load(face_cascade_name))
	printf("--(!)Error loading face cascade\n");

	cv::Mat cloud_mask, img_tmp;
	cv::Rect face;

	bool NANflag = true;

	while(true)
	{
		sem_wait(&lock_facedetector);
		sem_wait(&mutex_facedetector);

		// just to flush out some frames
		if (frame_number_global < 50.0 || NANflag)
		{
			if (!(countNonZero(img_rgb_global_f!=img_tmp) == 0))
			{	
				img_tmp.release();
				img_tmp = img_rgb_global_f.clone();

				// detecting the face.
				face = detectFaceAndEyes(img_rgb_global_f, face_cascade);

				// reducing the search area
				img_cloud_global_c(face).copyTo(cloud_mask);

				// getting the center point of face.
				pointCloudTrajectory(cloud_mask, single_point_face_global);
				NANflag = 
					(isnan(single_point_face_global[0]) ||
					 isnan(single_point_face_global[1]) ||
					 isnan(single_point_face_global[2]));
				cloud_mask.release(); 
			}
		}
		else continue;
 //ei parta ta non commented
		sem_post(&mutex_facedetector);
	}
	return 0;
}

// ============================================================================
// ACTION RECOGNITION
// ============================================================================
void* actionRecognition(void* arg)
{
	// [VARIABLE] **************************************************************
	std::string phrase_now	= "";
	std::string path;
	std::string PARSED_RES;
	std::string OUTPUT_RES;
	std::string object		= obj_name;
	std::string RESULT_DIR	=  PARENT + "/" + RESULT + "/" + object + "/";
	directoryCheck(RESULT_DIR);

    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%y%m%d%H%M%S", &tstruct);
	PARSED_RES = "parsed_output_";
	PARSED_RES += buf;
	PARSED_RES += ".txt";
	OUTPUT_RES = "output_";
	OUTPUT_RES += buf;
	OUTPUT_RES += ".txt";

	int loc_int = 100;
	int sec_int = 36;
	int filter_w = 5;

	bool nolabel = false;
	bool gauss = true;

	ReadFile RF;
	// ************************************************************** [VARIABLE] 

	// create a data container
	auto cdata = std::make_shared<CData>(object, loc_int, sec_int);

	/* Reading surface
	 * Reading action labels
	 * - reads the labels and initializes a zero list prediction/filter with the same length as the label
	 * Reading object specific labels
	 * - reads the object specific labels and saves them */
	path = PARENT + "/" + KB_DIR + "/";
	if (RF.ReadFileKB(path, cdata->KB)==EXIT_FAILURE)
	{return 0;}

	/* read object state if needed */
	path = PARENT + "/" + KB_DIR + "/";
	if (RF.ReadFileOS(path, cdata->OS)==EXIT_FAILURE)
	{return 0;}

	/* read parse message */
	path = PARENT + "/" + KB_DIR + "/";
	if (RF.ReadMsg(path, cdata->msg)==EXIT_FAILURE)
	{return 0;}

	// directory of learned data of a subject
	std::string dir_s = PARENT + "/" + EVAL + "/" + object + "/0";

	// read available location areas
	path  = dir_s + "/location_area.txt";
	if (RF.ReadFileLA(path, cdata->KB->AL(), cdata->G) == EXIT_FAILURE)
	{return 0;}

	// read available sector map
	path = 	dir_s + "/graph.txt";
	if (RF.ReadFileGraph(path, cdata->G) == EXIT_FAILURE)
	{return 0;}

	// directory to parsed message
	path = PARENT + "/" + RESULT + "/" + object + "/";
	directoryCheck(path);

	// create Test object
	auto T = std::make_shared<Test>(object, loc_int, sec_int, filter_w,
			cdata, path, true);

	// apply gauss filter
	if (gauss) { T->ApplyGauss(5,5); }

	// write window constraint
	path = 	dir_s + "/window.txt";
	if (T->WriteWindow(path)==EXIT_FAILURE) {return 0;}

	// Initialize
	T->TestInit();

	int c = 0;
	bool flag = true;
	while(true)
	{
		sem_wait(&lock_actionrecognition);
		sem_wait(&mutex_actionrecognition);

		if ( isnan(single_point_face_global[0])==false &&
			 isnan(single_point_face_global[1])==false &&
			 isnan(single_point_face_global[2])==false && flag)
		{
			Eigen::Vector4d face_parser(
					 single_point_face_global[0],
					-single_point_face_global[1],
					 single_point_face_global[2],
					 0.0);
			//if (object=="CUP")
			face_parser += Eigen::Vector4d(-0.1, 0, 0.1, -1);
			T->LAAdjust("FACE", face_parser);
			flag = false;
		}
 
		// 1. Filter
		T->FilterData(
				Eigen::Vector4d(
						-gPosition1.x,
						-gPosition1.y,
						 gPosition1.z,
						-2.0),
				*(cdata->pva),
				(int)contact_obj,
				*(cdata->contact));

		// 2. Prediction
		T->Predict();

		// 3. Get Data
		T->GetData((int)frame_number_global);
			
		
		for(auto val : cdata->AS->Goal())
	{		
			std::string abc= val.first;
			if (abc == "BOWL")
			 	probs[0]=val.second;
				
			 if (abc == "SHELF")
				probs[1]=val.second;
			 if (abc == "SINK")
				probs[2]=val.second;
			 if (abc == "BIN")
				probs[3]=val.second;
			 if (abc == "TABLE1")
				probs[4]=val.second;
			 if (abc == "TABLE2")
				probs[5]=val.second;
			 if (abc == "FACE")
				probs[6]=val.second;
			 if (abc == "DISHWASHER")
				probs[7]=val.second;	

			/*for(int i=0;i<8;i++)
			{
				std::cout<<"the value at "<<i<<"is : "<<probs[i]<<std::endl;
			}*/
			//std::cout<<"the value at sink is " <<probs[1]<<std::endl;
			

			//cout<<val.first<<":\t\t"<<val.second<<endl;
	}




		//if (contact_obj == true)		
		//{
		source_node=cdata->AS->Label1();
		//std::cout<<"source_node:\t"<<source_node<<std::endl;
		destination_node=cdata->AS->Label2();
		//std::cout<<"destination_node:\t"<<destination_node<<std::endl;

		//}
			// 4. Parse Data
		T->Parser(PARSED_RES, (int)frame_number_global, phrase_now);		
		phrase = phrase_now;

		//goals.push_back(cdata->AS->Goal());

		c++;
		if (c==30)
		{
			c = 0;
			// 5. Write Results
			T->WriteResult(OUTPUT_RES, RESULT_DIR, true);
		}

		sem_post(&mutex_actionrecognition);
		sem_post(&lock_tracker);
	}
	return 0;
}

// ============================================================================
// TTS
// ============================================================================
void* tts(void* arg)
{
	/*std::string phrase_last	= "";
	int heap_size = 210000;  // default scheme heap size
	int load_init_files = 1; // we want the festival init files loaded
	festival_initialize(load_init_files,heap_size);
	festival_eval_command("(voice_ked_diphone)");

	while(1)
	{
		sem_wait(&mutex_tts);
		if (phrase != phrase_last)
		{
			phrase_last = phrase;
			std::cout << phrase_last << std::endl;
			festival_say_text(phrase_last.c_str());
		}
		sem_post(&mutex_tts);
	}
	return 0;*/
}


// =============================================================================
// >>>>> MAIN <<<<<
// =============================================================================

int main(int argc, char *argv[])
{

	if (argc==1)
	{
		std::cerr << "Object file not given." << std::endl;
		std::cerr << "Exiting..............." << std::endl;
		return 0;
	}
	else if (argc==2)
	{
		std::cerr << "Object model file not given.                  " << std::endl;
		std::cerr << "File should be in ../config/pose_cache_xxx.txt" << std::endl;
		std::cerr << "Exiting......................................." << std::endl;
		return 0;
	}
	else if (argc==3)
	{
		std::cerr << "Threshold for hand contact not given. (200-500)" << std::endl;
		std::cerr << "Exiting........................................" << std::endl;
		return 0;
	}
	else if (argc==4)
	{
		obj_model = std::string(argv[1]);
		obj_name  = std::string(argv[2]);
		std::cout << "Using object model file : " << obj_model << std::endl;
		THRESHOLD = std::stoi(argv[3]);
	}
	else
	{
		std::cerr << "Too much arguments given." << std::endl;
		std::cerr << "Exiting.................." << std::endl;
		return 0;
	}

//	if (obj_name == "CUP") 	 	THRESHOLD = 250;
//	else if (obj_name == "SPG")	THRESHOLD = 200;
//	else if (obj_name == "APP")	THRESHOLD = 300;

//	cv::namedWindow("rgb");		cv::moveWindow("rgb",0,550);  
//	cv::namedWindow("depth");	cv::moveWindow("depth",0,0);
//	cv::namedWindow("rgb_o");	cv::moveWindow("rgb_o",0,550);  
//	cv::namedWindow("rgb_h");	cv::moveWindow("rgb_h",0,0);
//	cv::namedWindow("rgb_c");	cv::moveWindow("rgb_c",0,0);

	sem_init(&lock_tracker, 0, 3);
	sem_init(&lock_viewer, 0, 0);
	sem_init(&lock_contactdetector, 0, 0);
	sem_init(&lock_actionrecognition, 0, 0);
	sem_init(&lock_facedetector, 0, 0);

	sem_init(&mutex_tracker, 0, 1);
	sem_init(&mutex_viewer, 0, 1);
	sem_init(&mutex_contactdetector, 0, 1);
	sem_init(&mutex_actionrecognition, 0, 1);
	sem_init(&mutex_facedetector, 0, 1);
	sem_init(&mutex_tts, 0, 1);

	pthread_t 	thread_dbot,
				thread_opengl,
				thread_faceDetector,
				thread_contactDetector,
				thread_actionrecognition,
				thread_tts;

	pthread_attr_t attr;
	cpu_set_t cpus;
	pthread_attr_init(&attr);

	CPU_ZERO(&cpus);
	CPU_SET(1, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
	pthread_create(&thread_dbot, &attr, dbotthread, NULL);

	CPU_ZERO(&cpus); //eita
	CPU_SET(2, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
	pthread_create(&thread_opengl, &attr, openglthread, NULL);  //eita

//	CPU_ZERO(&cpus);
//	CPU_SET(3, &cpus);
//	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
//	pthread_create(&thread_obj, &attr, objectDetector, NULL);

//	CPU_ZERO(&cpus);
//	CPU_SET(4, &cpus);
//	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
//	pthread_create(&thread_hand, &attr, handDetector, NULL);

	CPU_ZERO(&cpus);
	CPU_SET(3, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
	pthread_create(&thread_contactDetector, &attr, contactDetector, NULL);

	CPU_ZERO(&cpus);
	CPU_SET(4, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
	pthread_create(&thread_faceDetector, &attr, faceDetector, NULL);

	CPU_ZERO(&cpus);
	CPU_SET(5, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
	pthread_create(&thread_actionrecognition, &attr, actionRecognition, NULL);

	CPU_ZERO(&cpus);
	CPU_SET(6, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);
	pthread_create(&thread_tts, &attr, tts, NULL);

	pthread_join(thread_dbot, NULL);
	pthread_join(thread_opengl, NULL);//eita
//	pthread_join(thread_obj, NULL);
//	pthread_join(thread_hand, NULL);
	pthread_join(thread_contactDetector, NULL);
	pthread_join(thread_faceDetector, NULL);
	pthread_join(thread_actionrecognition, NULL);
	pthread_join(thread_tts, NULL);


	return 0;
}



void thresh_callback(int, void* )
 {
   cv::Mat src_copy = src.clone();
	//imshow("src",src_copy);
   cv::Mat threshold_output;
   std::vector<std::vector<cv::Point> > contours;   //vector of vectors of type point
   std::vector<cv::Vec4i> hierarchy;		    //vector of 4d vectors
   	    //trying to find the convexity defects
	cv::namedWindow( "Hull demo", CV_WINDOW_NORMAL );
	//cv::namedWindow( "gesture_drawing", CV_WINDOW_NORMAL );
   /// Detect edges using Threshold

   cv::threshold( src_gray, threshold_output, thresh, 255, cv::THRESH_BINARY ); //takes only the pixels above the threshold value
	

   /// Find contours
   /// each detected contour is stored as a vector of points
   cv::findContours( threshold_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	// cv::findContours( threshold_output, contours, hierarchy,CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	
	//std::cout<<"contour size: "<<contours.size()<<std::endl;

   /// Find the convex hull object for each contour
   std::vector<std::vector<cv::Point> >hull( contours.size() );
   std::vector<std::vector<int> > hullsI(contours.size());	//ami add kortesi
  
   std::vector< std::vector<cv::Vec4i> > defects( contours.size() );
	
		
   for( int i = 0; i < contours.size(); i++ )
      {  
		convexHull( cv::Mat(contours[i]), hull[i], false );
		convexHull(cv::Mat(contours[i]), hullsI[i], false); 
    		if(hullsI[i].size() > 3 ) // You need more than 3 indices          
    		{
        		convexityDefects(contours[i], hullsI[i], defects[i]);
				
			
    		}
      }
		
	
   /// Draw contours + hull+ convexity defects results
   cv::Mat drawing = cv::Mat::zeros( threshold_output.size(), CV_8UC3 );
	
   for( int i = 0; i< contours.size(); i++ )
      {
	int count=0;
        cv::Scalar color = cv::Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
	//int count=0;
	double a=contourArea( contours[i],false);  //  Find the area of contour
	//std::cout<<"area of the contour "<<i<<" is : "<<a<<std::endl;
	if(a<9000 && a> 100)
	{//std::cout<<"area of the contour "<<i<<" is : "<<a<<std::endl;
	cv::Scalar color = cv::Scalar( 255, 255, 255 );  //setting white color
	cv::Scalar color_contour=cv::Scalar(0,255,0);
        cv::drawContours( drawing, contours, i, color_contour, 1, 8, std::vector<cv::Vec4i>(), 0, cv::Point() );
        cv::drawContours( drawing, hull, i, color, 1, 8, std::vector<cv::Vec4i>(), 0, cv::Point() );

	if(hullsI[i].size()>3 &&defects[i].size()>0)
	{ 
		//std::cout<<"size of defect vector: "<<defects.size()<<std::endl;
		//std::cout<<"size of defect vector at "<<i<<" :"<<defects[i].size()<<std::endl;
		 //std::cout<<"value of defects at "<<i<<"  :"<<defects[i][0]<<std::endl;
	  for (int ii = 0; ii < defects[i].size(); ii++)
              {
                  cv::Point p1 = contours[i][defects[i][ii][0]];
                  cv::Point p2 = contours[i][defects[i][ii][1]];
                  cv::Point p3 = contours[i][defects[i][ii][2]];
                 // cv::line(drawing, p1, p3, cv::Scalar(255, 0, 0), 2);
		  //cv::line(drawing, p2, p3, cv::Scalar(255, 0, 0), 2);

		double length= cv::norm(p1-p3);
		//std::cout<<"lenght value is : "<<length<<std::endl;
		if(length>20.0)  //this is a threshold I selected by checking a few diff lengths. This is not 100% accurate
			{	//cv::circle(drawing,p3,2,cv::Scalar(255,0,0),2,8,0);
				count++;
				
			}

			
                  //cv::line(drawing, p3, p2, cv::Scalar(255, 0, 0), 2);
			
              }

			cv::namedWindow( "Gesture", CV_WINDOW_NORMAL );
   			cv::resizeWindow( "Gesture", 600,600);
				if(count==0)
					{//std::cout<<"its a fist"<<std::endl;
						cv::Mat gesture = src.clone();
						
						 imshow( "Gesture", gesture );
						//cv::waitKey(1);
					}
				else if (count == 1)
					{//std::cout<<"A POINTER"<<std::endl;
						cv::Mat gesture = src.clone();
						
						 imshow( "Gesture", gesture );
						//cv::waitKey(1);

					}
				else if (count==5)
					{
						//std::cout<<"5 FINGERS"<<std::endl;
						 cv::Mat gesture = src.clone();
						
						 imshow( "Gesture", gesture );
						//cv::waitKey(1);
					
						//cv::destroyWindow("gesture");
					}
		
				
	}
		
	}
	
      }
	
	
	

   /// Show in a window
   cv::namedWindow( "Hull demo", CV_WINDOW_NORMAL );
   cv::resizeWindow( "Hull demo", 600,600);  //resizing the window, optional
   imshow( "Hull demo", drawing );
   cv::waitKey(300);
	
 }

















